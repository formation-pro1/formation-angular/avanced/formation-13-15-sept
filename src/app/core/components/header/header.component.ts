import { Component, OnInit } from '@angular/core';
import { VersionService } from '../../services/version.service';
import {AuthService} from "../../../shared/auth.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public version!: number;

  public currentUser$!: Observable<string | null>;

  constructor(private versionService: VersionService,
              private authService: AuthService) {
    this.versionService.numVersion$.subscribe((next) => this.version = next);
   }

  ngOnInit(): void {
    this.currentUser$ = this.authService.getCurrentUser$().pipe(
      map(user => user !== null ? user.login : null)
    );
  }

}
