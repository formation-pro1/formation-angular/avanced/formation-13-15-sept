import {Component, OnInit} from '@angular/core';
import {VersionService} from '../../services/version.service';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public version!: number;

  constructor(private versionService: VersionService,
              private translateService: TranslateService) {
    this.versionService.numVersion.subscribe((next) => this.version = next);

    this.translateService.setDefaultLang('fr');
    this.translateService.addLangs(['en', 'fr']);
  }

  ngOnInit(): void {
    this.getLangs();
  }

  public setCurrentLang(langWished: string): void {
    this.translateService.use(langWished);
  }

  public getLangs(): string[] {
    return this.translateService.getLangs();
  }
}
