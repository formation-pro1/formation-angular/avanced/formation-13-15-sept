import {ClientState} from "../enums/state-client";

export interface Client {
  id: number;
  name: string;
  email: string;
  state: ClientState;
}
