import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Client} from "../../core/interfaces/client";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private urlApi = environment.urlApi;

  constructor(
    private http: HttpClient
  ) { }

  list(): Observable<Client[]> {
    return this.http.get<Client[]>(`${this.urlApi}/clients`);
  }

  get(id: number): Observable<Client> {
    return this.http.get<Client>(`${this.urlApi}/clients/${id}`);
  }

  update(data: Client): Observable<Client> {
    return this.http.put<Client>(`${this.urlApi}/clients/${data.id}`, data);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.urlApi}/clients/${id}`);
  }

  create(data: any): Observable<Client> {
    return this.http.post<Client>(`${this.urlApi}/clients`, data);
  }
}
