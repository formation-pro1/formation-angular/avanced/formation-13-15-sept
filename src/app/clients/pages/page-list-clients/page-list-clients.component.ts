import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {ClientState} from "../../../core/enums/state-client";
import {Client} from "../../../core/interfaces/client";
import {ClientService} from "../../services/client.service";

@Component({
  selector: 'app-page-list-clients',
  templateUrl: './page-list-clients.component.html',
  styleUrls: ['./page-list-clients.component.scss']
})
export class PageListClientsComponent {
  headers = ['Actions', 'Name', 'Email', 'State'];
  clientsStates = Object.values(ClientState);

  constructor(
    private router: Router,
    private clientService: ClientService
  ) {}

  ngOnInit(): void {
  }

  public navigateToEdit(id: number): void {
    this.router.navigate(['clients', 'edit', id]);
  }

  public update(state: string, client: any) {
    this.clientService.update({...client, state: ClientState.ACTIVE});
  }
}
