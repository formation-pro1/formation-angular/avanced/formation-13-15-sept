import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Client} from "../../../core/interfaces/client";
import {ClientState} from "../../../core/enums/state-client";

@Component({
  selector: 'app-form-client',
  templateUrl: './form-client.component.html',
  styleUrls: ['./form-client.component.scss']
})
export class FormClientComponent implements OnInit {

  form!: FormGroup;
  @Input() initClient = {name: '', email: '', state: 'Active'};
  @Output() submitted: EventEmitter<Client> = new EventEmitter<Client>();
  clientsStates = Object.values(ClientState);

  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
        name: [this.initClient.name, Validators.required],
        email: [this.initClient.email, Validators.email],
        state: this.initClient.state
      }
    );
  }

  onSubmit() {
    this.submitted.emit(this.form.value);
  }

}
