import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map, takeUntil, tap} from 'rxjs/operators';
import {StateOrder} from 'src/app/core/enums/state-order';
import {Order} from 'src/app/core/models/order';
import {OrdersService} from '../../services/orders.service';
import {select, Store} from "@ngrx/store";
import {AppState} from "../../../store";
import {selectOrdersList, selectOrdersLoading} from "../../../store/selectors/order.selectors";
import {deleteOrder, loadOrders, selectOrder} from "../../../store/actions/order.actions";

@Component({
  selector: 'app-page-list-orders',
  templateUrl: './page-list-orders.component.html',
  styleUrls: ['./page-list-orders.component.scss'],
})
export class PageListOrdersComponent implements OnInit {

  public ordersCollection$!: Observable<Order[]>;
  public possibleStates = Object.values(StateOrder);

  public isLoading$!: Observable<boolean>

  public headers = [
    'Action',
    'Type',
    'Client',
    'NbJours',
    'Tjm HT',
    'Total HT',
    'Total TTC',
    'State',
  ];

  public myTitle = 'Liste des commandes';

  constructor(
    private store: Store<AppState>,
    private ordersService: OrdersService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    console.log(this.possibleStates);
  }

  ngOnInit(): void {
    this.store.dispatch(loadOrders());

    this.ordersCollection$ = this.store.pipe(
      tap(value => console.log(value)),
      select(selectOrdersList),
      map((tableau: Order[]) => {
        return tableau.map((element) => {
          return new Order(element);
        })
      })
    );

    this.isLoading$ = this.store.pipe(
      select(selectOrdersLoading)
    );
  }

  public changeTitle(): void {
    this.myTitle = 'Liste des commandes ' + Math.random();
  }

  public changeState(item: Order, selectEvent: any): void {
    this.ordersService.changeState(item, selectEvent.target.value).subscribe(
      (res) => {
        console.log(res);
      }
    );
  }

  public navigateToEdit(order: Order): void {
    this.store.dispatch(selectOrder({payload: order}))
    this.router.navigate(['orders', 'edit', order.id]);
  }

  public deleteOrder(order: Order): void {
    this.store.dispatch(deleteOrder({payload: order}));
  }
}
