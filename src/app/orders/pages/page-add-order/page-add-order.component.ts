import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Order } from 'src/app/core/models/order';
import { OrdersService } from '../../services/orders.service';
import {FormOrderComponent} from "../../components/form-order/form-order.component";
import {CheckForm} from "../../../shared/guards/check-form.guard";
import {AppState} from "../../../store";
import {Store} from "@ngrx/store";
import {createOrder} from "../../../store/actions/order.actions";

@Component({
  selector: 'app-page-add-order',
  templateUrl: './page-add-order.component.html',
  styleUrls: ['./page-add-order.component.scss'],
})
export class PageAddOrderComponent extends CheckForm<FormOrderComponent> implements OnInit {

  public item = new Order();
  constructor(private orderService: OrdersService,
              private store: Store<AppState>,
              private router: Router) {
    super();
  }

  ngOnInit(): void {
  }

  saveOrder(order: Order): void {
    this.store.dispatch(createOrder({payload: order}));
  }
}
