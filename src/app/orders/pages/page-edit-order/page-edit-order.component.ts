import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {Order} from 'src/app/core/models/order';
import {FormOrderComponent} from "../../components/form-order/form-order.component";
import {CheckForm} from "../../../shared/guards/check-form.guard";
import {AppState} from "../../../store";
import {select, Store} from "@ngrx/store";
import {selectSelectedOrder} from "../../../store/selectors/order.selectors";
import {saveOrder} from "../../../store/actions/order.actions";

@Component({
  selector: 'app-page-edit-order',
  templateUrl: './page-edit-order.component.html',
  styleUrls: ['./page-edit-order.component.scss']
})
export class PageEditOrderComponent extends CheckForm<FormOrderComponent> implements OnInit {

  public order$!: Observable<Order | null>;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<AppState>) {
    super();
  }

  ngOnInit(): void {
    this.order$ = this.store.pipe(
      select(selectSelectedOrder)
    );
  }

  public updateOrder(order: Order): void {
    this.store.dispatch(saveOrder({payload: order}));
  }
}
