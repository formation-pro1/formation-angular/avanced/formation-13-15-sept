import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {
  createOrder, createOrderFailure,
  createOrderSuccess, deleteOrder, deleteOrderFailure, deleteOrderSuccess,
  loadOrders,
  loadOrdersFailure,
  loadOrdersSuccess, saveOrder, saveOrderFailure, saveOrderSuccess
} from "../actions/order.actions";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {OrdersService} from "../../orders/services/orders.service";
import {Order} from "../../core/models/order";
import {of} from "rxjs";
import {Router} from "@angular/router";


@Injectable()
export class OrderEffects {

  // CREATION
  loadOrders$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loadOrders),
        switchMap((action) => this.orderService.orders),
        map((orders: Order[]) => loadOrdersSuccess({orders: orders})),
        catchError((error) => of(loadOrdersFailure({error: error})))
      )
  );

  createOrder$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createOrder),
        switchMap((action) => this.orderService.add(action.payload)),
        map((order: Order) => createOrderSuccess({payload: order})),
        catchError((error) => of(createOrderFailure({error: error})))
      )
  );

  updateOrSaveOrCreateOrderSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(createOrderSuccess, saveOrderSuccess),
        tap(() => this.router.navigate(['orders']))
      ),
    {
      dispatch: false
    }
  );

  // ------------------------------------

  // EDITION
  saveOrder$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(saveOrder),
        switchMap((action) => this.orderService.update(action.payload)),
        map((order: Order) => saveOrderSuccess({payload: order})),
        catchError((error) => of(saveOrderFailure({error: error})))
      )
  );

  // ------------------------------------

  // SUPPRESSION
  deleteOrder$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteOrder),
        switchMap((action) => this.orderService.delete(action.payload)),
        map((order: Order) => deleteOrderSuccess()),
        catchError((error) => of(deleteOrderFailure({error: error})))
      )
  );

  deletedOrder$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteOrderSuccess),
        map(() => loadOrders()),
      )
  );


  constructor(private actions$: Actions,
              private router: Router,
              private orderService: OrdersService) {
  }

}


//switchMap()
//concatMap()
//mergeMap()
//exhaustMap()
