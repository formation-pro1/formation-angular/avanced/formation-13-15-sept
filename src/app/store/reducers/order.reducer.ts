import {createReducer, on} from '@ngrx/store';
import {Order} from "../../core/models/order";
import * as OrderActions from "../actions/order.actions";

export const orderFeatureKey = 'order';

export interface State {
  orders: Order[];
  selectedOrder: Order | null;
  loaded: boolean;
  loading: boolean;
}

export const initialState: State = {
  orders: [],
  selectedOrder: null,
  loaded: false,
  loading: false,
};

export const reducer = createReducer(
  initialState,
  on(OrderActions.loadOrders, state => ({...state, loading: true})),
  on(OrderActions.loadOrdersSuccess, (state, {orders}) => ({...state, orders: orders, loading: false, loaded: true})),
  on(OrderActions.loadOrdersFailure, (state, {error}) => {
    console.log(error);
    return ({...state, loading: false, loaded: false})
  }),

  on(OrderActions.createOrder, (state) => ({...state, loading: true})),
  on(OrderActions.createOrderSuccess, (state, {payload}) => ({
    ...state,
    orders: [...state.orders, payload],
    loaded: true,
    loading: false
  })),
  on(OrderActions.createOrderFailure, (state, {error}) => {
    console.log(error);
    return ({...state, loading: false, loaded: false})
  }),


  on(OrderActions.selectOrder, (state, {payload}) => ({...state, selectedOrder: payload})),
  on(OrderActions.saveOrder, (state) => ({...state, loading: true})),
  on(OrderActions.saveOrderSuccess, (state, {payload}) => ({
    ...state,
    orders: [...state.orders, payload],
    selectedOrder: null,
    loaded: true,
    loading: false
  })),
  on(OrderActions.saveOrderFailure, (state, {error}) => {
    console.log(error);
    return ({
      ...state,
      loading: false,
      loaded: false
    })
  }),


  on(OrderActions.deleteOrder, (state) => ({...state, loading: true})),
  on(OrderActions.deleteOrderSuccess, (state) => ({
    ...state,
    loading: false
  })),
  on(OrderActions.deleteOrderFailure, (state, {error}) => {
    console.log(error);
    return ({...state, loading: false})
  }),
);

