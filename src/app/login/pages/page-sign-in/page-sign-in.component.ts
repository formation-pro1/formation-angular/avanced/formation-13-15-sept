import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../shared/auth.service";

@Component({
  selector: 'app-page-sign-in',
  templateUrl: './page-sign-in.component.html',
  styleUrls: ['./page-sign-in.component.scss']
})
export class PageSignInComponent {
  form: FormGroup = this.fb.group({
    login: ['', [Validators.required]],
  });

  constructor(
    public authService: AuthService,
    private fb: FormBuilder
  ) {
  }

  login() {
    this.authService.login(this.form.value.login)
  }
}
