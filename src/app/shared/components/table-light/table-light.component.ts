import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-table-light',
  templateUrl: './table-light.component.html',
  styleUrls: ['./table-light.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class TableLightComponent {

  @Input()
  public headers!: string[];

  constructor() {}

}
