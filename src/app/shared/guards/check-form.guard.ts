import {Directive, Injectable, ViewChild} from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';


export abstract class HasFormDirty {
  abstract isFormDirty(): boolean;
}

@Directive()
export abstract class CheckForm<T extends HasFormDirty> {

  @ViewChild('form')
  private formComponent!: T;

  public isFormDirty(): boolean {
    return this.formComponent.isFormDirty();
  }
}


@Injectable({
  providedIn: 'root'
})
export class CheckFormGuard implements CanDeactivate<CheckForm<any>> {
  canDeactivate(
    component: CheckForm<any>,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // Comment je vérifie que je peux fermer mon composant ?
    if (component.isFormDirty()) {
      if (confirm("Voulez vous vraiment quitter le formulaire ?")) {
        return true;
      }
      return false;
    }
    return true;
  }

}
