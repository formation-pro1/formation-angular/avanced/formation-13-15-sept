import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {BehaviorSubject, Observable} from "rxjs";

export interface User {
  login: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);

  constructor(
    public router: Router,
  ) {
  }

  public getCurrentUser$(): Observable<User | null> {
    return this.user.asObservable();
  }

  public login(login: string) {
    this.user.next({login});
    this.router.navigate(['/clients']);
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/sign-in']);
  }
}
