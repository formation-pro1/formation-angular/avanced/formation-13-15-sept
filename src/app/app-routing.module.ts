import { NgModule } from '@angular/core';
import {
  PreloadAllModules,
  RouterModule,
  Routes,
} from '@angular/router';
import {CustomPreloadingService} from "./custom-preloading.service";
import {AuthGuard} from "./shared/guards/auth.guard";

const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  {
    path: 'orders',
    canLoad: [AuthGuard],
    loadChildren: () =>
      import('./orders/orders.module').then((m) => m.OrdersModule),
    data: {preload: true}
  },
  {
    path: 'clients',
    canLoad: [AuthGuard],
    loadChildren: () =>
      import('./clients/clients.module').then((m) => m.ClientsModule),
    data: {preload: true}
  },
  {
    path: '**',
    loadChildren: () =>
      import('./page-not-found/page-not-found.module').then(
        (m) => m.PageNotFoundModule
      ),
    data: {preload: false}
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
